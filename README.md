LogFileReader

# Usage
`$ sbt`
`run file1 file2 ...`

# Thoughts and Considerations
Since the bulk of the assignment was concerned around memory usage and we have to rip through so many files I had to balance memory usage, with easy of looksup. I.e we have to balance Execution time and memory
To faciliate this I built `HashHeap` and `Path`. `Path` holds the view count and path, but is sorted by the view count and then the Path. Equality is checked only by the full path. `HashHeap` holds a hash of all the deep paths ever 
seen and a TreeSet of the current time trees and the last deep path that got evicted. 
This way if we update a deep path we don't have to do the expensive operation of adding to a TreeSet (with takes N time since we have to find by the path first and trees are sorted by the value of the Path).
While in general TreeSet (whcih is based on RedBlack trees) are LogN time, because we always limit the size, getting all the top values happens in constant time. 

To minimize memory usage, I compact the main key (which will always be the number of users) to Longs. All IPv4 values can be expressed as long, so converting them saves on usage. 
In profiling this seems to also help with the GC after the JVM warms up. 

Also the overall memory usage is based on the max number of Users seen. Since this will always be bigger than any other thing we're looking at (i.e paths or the deep paths), the memory usage is mostly constrained by this. 


# Testing

There are unit tests. There is also a python script to generate an ~ 3.3GB file that you can use to test. On my local machine with an SSD I got about 140s through all of it. 

# Overall thoughts

In general this was a challenging project. Making sure it was designed well and Scalactic was fun to figure out and due, and I learned a new trick on minimizing cyclomatic complexity with using case statements on tuples to essentially mimic truth tables. 

If I had to add anything, I'd add more tests. Though, I think in general, all good software good probably use more tests. 

Thanks for giving me this opportunity. I've appreciated the time and effort everyone has put in to interview me. 

Dev Gupta
