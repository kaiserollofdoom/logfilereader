package org.kaiserollofdoom.logfilereader.collection

case class Path(path: String, viewCount: Long = 1) extends Ordered[Path]{
  override def compare(that: Path): Int = {
    if(that == this) 0
    else{
      val primary = -1 * (this.viewCount compare that.viewCount)
      if(primary == 0) this.path compare that.path else primary
    }
  }

  override def canEqual(that: Any): Boolean = that.isInstanceOf[Path]

  override def equals(that: Any): Boolean = {
    that match {
      case that: Path => that.canEqual(this) && this.path == that.path
      case _ => false
    }
  }

  override def hashCode(): Int = this.path.hashCode
}
