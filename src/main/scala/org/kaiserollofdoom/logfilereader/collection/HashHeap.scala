package org.kaiserollofdoom.logfilereader.collection

import scala.annotation.tailrec

case class HashHeap(deepPathMap: Map[String, Long], topDeepPaths: Set[Path], lastEvicted: Path, topN : Int = 10) {
  def +(deepPath: Path): HashHeap = {
    //update it get the value and see if it's greater than the last evicted
    val pathOption = deepPathMap.get(deepPath.path)
    val pathValue = pathOption match {
      case Some(x) => x
      case None => 0
    }
    //is the deep path greater than the last evicted
    val updatedDeepPath = Path(deepPath.path, deepPath.viewCount + pathValue)
    val addToDeepestPaths = updatedDeepPath.viewCount > lastEvicted.viewCount || topDeepPaths.size < topN
    val evictPath = topDeepPaths.size == topN
    //get the evicted path and the new top tree
    val (newTree,evicted) = (addToDeepestPaths, evictPath) match {
     case(true, true) =>
       //get the top elements
       val evict = topDeepPaths.last
       val topElems = topDeepPaths.take(topN - 1)
       //remove the current path if it's still in the top deep paths
       val removeElementIfThere = topElems.filterNot(p => p.path == deepPath.path)
       val addedItem = removeElementIfThere + updatedDeepPath
       (addedItem, evict)
     case(true, false) =>
       val removeElementIfThere = topDeepPaths.filterNot(p => p.path == deepPath.path)
       val addedItem = removeElementIfThere + updatedDeepPath
       (addedItem, lastEvicted)
     case(false, _) => (topDeepPaths, lastEvicted)
    }
    //return a new hashheap with all the necessary updates
    (addToDeepestPaths, evictPath) match {
      case (true, true) =>
        HashHeap(deepPathMap + (updatedDeepPath.path -> updatedDeepPath.viewCount), newTree, evicted, topN)
      case (true, false) => HashHeap(deepPathMap + (updatedDeepPath.path -> updatedDeepPath.viewCount), newTree, evicted, topN)
      case (false, _) => HashHeap(deepPathMap + (updatedDeepPath.path -> updatedDeepPath.viewCount), newTree, evicted, topN)
    }
  }


  def +(deepPaths: Seq[Path]): HashHeap = {
    @tailrec
    def helper(hashHeap: HashHeap, path: Seq[Path]): HashHeap ={
      path match{
        case Nil => hashHeap
        case x :: tail => helper(hashHeap+x, tail)
      }
    }
    helper(this, deepPaths)
  }

}
