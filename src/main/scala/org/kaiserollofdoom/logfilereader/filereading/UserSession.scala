package org.kaiserollofdoom.logfilereader.filereading

case class UserSession(lastSeenTimestamp: Long, views: Long = 1, deepOn: Long = 3, invalidAfter: Long = 600, isValidSession: Boolean = false) {

  val isDeep: Boolean = views > deepOn

  def checkSession(newSeenTimestamp: Long): UserSession ={
    val valid = math.abs(newSeenTimestamp - lastSeenTimestamp) < invalidAfter && (newSeenTimestamp > lastSeenTimestamp)
    if (valid) {
      UserSession(lastSeenTimestamp = newSeenTimestamp, views = this.views + 1, isValidSession = true)
    } else {
      UserSession(lastSeenTimestamp = newSeenTimestamp)
    }
  }

}
