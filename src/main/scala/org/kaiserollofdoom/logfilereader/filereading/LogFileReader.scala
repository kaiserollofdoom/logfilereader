package org.kaiserollofdoom.logfilereader.filereading

import java.io.IOException

import org.kaiserollofdoom.logfilereader.collection.{HashHeap, Path}
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.annotation.tailrec
import scala.collection.immutable.TreeSet
import scala.io.Source

object LogFileReader {

  def main(args: Array[String]): Unit = {
    val (topHashHeap, _) = processFiles(args.toList)
    topHashHeap.topDeepPaths.foreach(println)
  }

  def compactIp(ip: String) : Long = ip.split("\\.").foldLeft(0L)((c,n) => c*256+n.toLong)

  def processFiles(filePaths: List[String]) : (HashHeap, Map[Long, UserSession]) = {
    /*
    Iterate through all the files and return the final results
     */
    @tailrec
    def helper(filePaths: List[String], sessionCache: Map[Long, UserSession], hashHeap: HashHeap): (HashHeap, Map[Long, UserSession]) = filePaths match {
      case Nil => (hashHeap, sessionCache)
      case x :: tail =>
        val (returnableHashHeap, returnableSessionCache) = processFile(x, sessionCache, hashHeap)
        helper(tail, returnableSessionCache, returnableHashHeap)
    }
    helper(filePaths, Map[Long, UserSession](), HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0)))
  }

  def processFile(filePath: String, sessionCache: Map[Long, UserSession], hashHeap: HashHeap): (HashHeap, Map[Long, UserSession]) = {
    /*
    Open and tailrec through the line iterator from the file and finally close the file
     */
    val reader = Source.fromFile(filePath)
    @tailrec
    def helper(lineIterator : Iterator[String], hashHeap: HashHeap, sessionCache: Map[Long, UserSession]): (HashHeap, Map[Long, UserSession]) ={
      if(!lineIterator.hasNext){
        (hashHeap, sessionCache)
      }
      else{
        val line = lineIterator.next
        //convert the line to user session
        val lineEither = decode[LogLineIntermediate](line)
        //does the user live in the session cache
        val logLine = lineEither match {
          case Right(x) => LogLine(x.ts, ip=compactIp(x.ip), path=Path(x.path))
          case Left(_) => LogLine(timestamp = -1, ip = compactIp("0.0.0.0"), path = Path(""))
        }
        val sessionOption = sessionCache.get(logLine.ip)
        val validSession = sessionOption match {
          case Some(x) => x.checkSession(logLine.timestamp)
          case _ => UserSession(lastSeenTimestamp = logLine.timestamp)
        }
        // we have the session let's check if the session is valid
        val recurHeap = (validSession.isValidSession, validSession.isDeep) match {
          case (true, true) => hashHeap + logLine.path
          case (true, false) => hashHeap
          case(false, _) => hashHeap
        }
        val newCache = sessionCache + (logLine.ip -> validSession)
        helper(lineIterator, recurHeap, newCache)
      }
    }
    val (returnableHashHeap, returnableSessionCache) = helper(reader.getLines, hashHeap, sessionCache)
    reader.close
    (returnableHashHeap, returnableSessionCache)
  }

}
