package org.kaiserollofdoom.logfilereader.filereading

import org.kaiserollofdoom.logfilereader.collection.Path

case class LogLine(timestamp: Long, ip: Long, path: Path)

case class LogLineIntermediate(ts: Long, ip: String, path: String)


