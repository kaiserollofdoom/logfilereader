package org.kaiserollofdoom.logfilereader.filereading

import org.scalatest.FlatSpec

class TestUserSession extends FlatSpec{

  "UserSesssion " should "be valid after update checks within 10 minutes (600 seconds)" in {
    var userSession = UserSession(lastSeenTimestamp = 0)
    for(i <- 300 to 1200 by 300) {
      userSession = userSession.checkSession(i)
    }
    assert(userSession.isDeep)
    assert(userSession.isValidSession)
    assert(userSession.views == 5)
  }

  "UserSesssion " should "be invalid after update checks after 10 minutes (600 seconds)" in {
    var userSession = UserSession(lastSeenTimestamp = 0)
    for(i <- 0 to 900 by 300) {
      userSession = userSession.checkSession(i)
    }
    userSession = userSession.checkSession(1501)
    assert(!userSession.isDeep)
    assert(!userSession.isValidSession)
    assert(userSession.views == 1)
  }




}
