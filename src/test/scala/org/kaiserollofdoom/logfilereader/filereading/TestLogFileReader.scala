package org.kaiserollofdoom.logfilereader.filereading

import org.kaiserollofdoom.logfilereader.collection.{HashHeap, Path}
import org.scalatest.FlatSpec

import scala.collection.immutable.TreeSet

class TestLogFileReader extends FlatSpec {

  "LogFileReader" should "return the correct top N from one file" in {
    val (hashHeap, sessionCache) = LogFileReader.processFile("./src/test/resources/log_file.out", Map[Long, UserSession](), HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0)))
    assert(hashHeap.deepPathMap.size == 2)
    assert(hashHeap.topDeepPaths.size == 2)
    assert(sessionCache.size == 3)
  }

  "LogFileReader" should "return the correct topN from one file even if that same file is passed in multiple times" in {
    val (hashHeap, sessionCache) = LogFileReader.processFiles(List("./src/test/resources/log_file.out","./src/test/resources/log_file.out"))
    assert(hashHeap.deepPathMap.size == 2)
    assert(hashHeap.topDeepPaths.size == 2)
    assert(sessionCache.size == 3)
  }

}
