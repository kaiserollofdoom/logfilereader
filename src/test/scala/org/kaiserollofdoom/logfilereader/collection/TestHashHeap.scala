package org.kaiserollofdoom.logfilereader.collection

import org.kaiserollofdoom.logfilereader.collection
import org.scalatest.FlatSpec

import scala.collection.immutable.TreeSet

class TestHashHeap extends FlatSpec{

  "HashHeaps" should "keep top values" in {
    val hashHeap = HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0))
    val testHashHeap = (hashHeap + Path("/foo/bar")) + Path("/foo/bar")
    assert(testHashHeap.topDeepPaths.size == 1)
    assert(testHashHeap.topDeepPaths.head == Path("/foo/bar", 2))
    val anotherTestHashHeap = testHashHeap + Path("/boo/baz", 10)
    assert(anotherTestHashHeap.topDeepPaths.size == 2)
    assert(anotherTestHashHeap.topDeepPaths.head == Path("/boo/baz", 10))
  }

  "HashHeaps" should "correctly evict items on the 4th item if topN is 3" in {
    val hashHeap = collection.HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0), 3)
    val finishHeap = hashHeap + List(Path("/foo/bar"), Path("/boo/baz"), Path("/youtube"), Path("/winner", 3))
    assert(finishHeap.lastEvicted == Path("/youtube"))
    assert(finishHeap.topDeepPaths.size == 3)
  }

  "HashHeaps" should "update the path list when a 100 inserts are made and only keep 2" in {
    val paths = List.fill(100)(Path("/foo/bar")) ++ List.fill(90)(Path("/boo/baz"))  ++ List.fill(8)(Path("/youtube")) ++ List.fill(2)(Path("/youtube"))
    val hashHeap = collection.HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0), 2)
    val finishedHashHeap = hashHeap+paths
    assert(finishedHashHeap.deepPathMap.size == 3)
    assert(finishedHashHeap.topDeepPaths.size == 2)
    assert(finishedHashHeap.topDeepPaths.head == Path("/foo/bar"))
    assert(finishedHashHeap.topDeepPaths.tail.head == Path("/boo/baz"))
    assert(finishedHashHeap.topDeepPaths.head.viewCount == 100)
    assert(finishedHashHeap.topDeepPaths.tail.head.viewCount == 90)
  }

  "HashHeaps" should "only have 1 path on duplicates" in {
    val paths = Iterator.fill(28650923)(Path("/foo/bar")) ++ Iterator.fill(28649077)(Path("/boo/baz"))
    val hashHeap = HashHeap(Map[String, Long](), TreeSet[Path](), Path("", 0))
    def help(it: Iterator[Path], hashHeap: HashHeap): HashHeap ={
     if(it.hasNext) {
        val newHeap = hashHeap + it.next
        help(it, newHeap)
     }else{
        hashHeap
     }
    }
    val ultimateHeap = help(paths, hashHeap)
    assert(ultimateHeap.topDeepPaths.size == 2)

  }



}
