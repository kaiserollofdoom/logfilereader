import json
import random
from random import randint

paths = ["/foo/bar", "/boo/baz"]
ips = set()
while len(ips) < 100000 :
    ips.add('.'.join([str(randint(0, 255)) for x in range(4)]))

'.'.join([str(randint(0,255)) for x in range(4)])
ips_in_file = 0
with open('profile_file.out', 'w') as file:
    for i in range(0,172800,300):
        for ip in ips:
            ips_in_file += 1
            line_out = {"ts": i, "ip":ip, "path": random.choice(paths)}
            file.write(json.dumps(line_out))
            file.write("\n")
            if ips_in_file % 10:
                print(ips_in_file)
        ips_in_file = 0